package controllers

import (
	"os"
	"log"
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type Config struct {
	User     string
	Password string
	Host	 string
	Port     string
	Protocol string
	Driver   string
	Database string
}

type ServicePoint struct {
	ID	           int
	Name               string
	ConsumerKey        string
	ConsumerSecret     string
	Token	           string
	PickupLocationGUID string
}

type Handle struct {
	db *sql.DB
}

func EnvConfig() *Config {
	return &Config{
		User: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Host: os.Getenv("DB_HOST"),
		Port: os.Getenv("DB_PORT"),
		Protocol: os.Getenv("DB_PROTOCOL"),
		Driver: os.Getenv("DB_DRIVER"),
		Database: os.Getenv("DB_NAME"),
	}
}

func (cfg *Config) Handle() (*Handle, error) {
	fmt.Printf("%#v\n", cfg)
	s := fmt.Sprintf("%v:%v@%v(%v:%v)/%v", cfg.User, cfg.Password, cfg.Protocol, cfg.Host, cfg.Port, cfg.Database)
	fmt.Println(s)
	db, err := sql.Open(cfg.Driver, s)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return &Handle{db: db}, nil
}

func (h *Handle) ServicePoint(sp string) (*ServicePoint, error) {
	servicePoint := &ServicePoint{}

	// prepare statement
	stmt, err := h.db.Prepare("SELECT id, name, consumer_key, consumer_secret, token, pickup_location_guid FROM service_points WHERE id = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// query
	rows, err := stmt.Query(sp)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// iterate over rows
	for rows.Next() {
		err := rows.Scan(
			&servicePoint.ID,
			&servicePoint.Name,
			&servicePoint.ConsumerKey,
			&servicePoint.ConsumerSecret,
			&servicePoint.Token,
			&servicePoint.PickupLocationGUID,
		)
		if err != nil {
			return nil, err
		}
		log.Println(
			servicePoint.ID,
			servicePoint.Name,
			servicePoint.ConsumerKey,
			servicePoint.ConsumerSecret,
			servicePoint.Token,
			servicePoint.PickupLocationGUID,
		)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return servicePoint, nil
}
