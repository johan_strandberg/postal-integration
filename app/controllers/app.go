package controllers

import (
	"fmt"
	"log"
	"os"
	"time"
	"strings"
	"github.com/revel/revel"
	"github.com/revel/revel/cache"
	"github.com/urbitassociates/postnord-direct/app/clients/urbit"
	"github.com/urbitassociates/postnord-direct/app/clients/postnord"
)

type App struct {
	*revel.Controller
}

const (
	ERROR_COLLECT_CODE_API = 1
	ERROR_DB_CFG = 2
	ERROR_RETAILER_QUERY = 3
	ERROR_CHECKOUT = 4
	ERROR_NO_CHECKOUT_URL = 5
	ERROR_CHECKOUT_STATUS = 6
)

func (a App) Index(id string, sp string) revel.Result {
	m := map[string]string{
		"environment": "Stage",
		"action":      "Initialize",
		"success":     "true",
		"valid":       "true",
	}

	if id != "" && sp != "" {
		m["checkoutURL"] = fmt.Sprintf("/checkout?id=%v&sp=%v", id, sp)
	}

	for k, v := range m {
		a.RenderArgs[k] = v
	}

	return a.Render()
}

func (a *App) Checkout(id string, sp string) revel.Result {
	appURL := os.Getenv("APP_BASE_URL")

	if id == "" || sp == "" {
		return a.RenderTemplate("App/Index.html")
	}

	// get collect code from postnord based on parcel ID
	ac := postnord.NewAuthConfig()
	cc, e := ac.CollectCode(id)
	if e != nil {
		log.Println(e)
		a.RenderArgs["action"] = "Error"
		a.RenderArgs["errorCode"] = ERROR_COLLECT_CODE_API
		return a.RenderTemplate("App/Index.html")
	}
	//cc := "97SE6MY9GP"

	dbcfg := EnvConfig()
	h, e := dbcfg.Handle()
	if e != nil {
		log.Println(e)
		a.RenderArgs["action"] = "Error"
		a.RenderArgs["errorCode"] = ERROR_DB_CFG
		return a.RenderTemplate("App/Index.html")
	}

	// fetch retailer from cache if available, or use database and update cache
	var servicePoint *ServicePoint
	if e := cache.Get("sp_"+sp, &servicePoint); e != nil {
		servicePoint, e = h.ServicePoint(sp)
		if e != nil {
			log.Println(e)
			a.RenderArgs["action"] = "Error"
			a.RenderArgs["errorCode"] = ERROR_RETAILER_QUERY
			return a.RenderTemplate("App/Index.html")
		}
		go cache.Set("sp_"+sp, servicePoint, 30*time.Minute)
	}

	// create checkout
	cocfg := &urbit.CheckoutConfig{
		ArticleRefID:       fmt.Sprintf("POSTNORD_%v_%v", cc, id),
		CancelURL:          fmt.Sprintf("%v/error?id=%v&sp=%v", appURL, id, sp),
		ReturnURL:          fmt.Sprintf("%v/thankyou?id=%v&sp=%v", appURL, id, sp),
		OrderID:            id,
		ArticleDescription: "Postpaket",
		Token:              servicePoint.Token,
		ConsumerSecret:     servicePoint.ConsumerSecret,
	}
	co, e := cocfg.Checkout()
	if e != nil {
		log.Println(e)
		a.RenderArgs["action"] = "Error"
		a.RenderArgs["errorCode"] = ERROR_CHECKOUT
		return a.RenderTemplate("App/Index.html")
	}

	// Redirect to Click&Get Checkout if URL is in response
	if co.Location != "" {
		return a.Redirect(co.Location)
	}
	log.Println(e)
	a.RenderArgs["action"] = "Error"
	a.RenderArgs["errorCode"] = ERROR_NO_CHECKOUT_URL
	return a.RenderTemplate("App/Index.html")
}

func (a *App) Thankyou(id string, sp string) revel.Result {
	if id == "" || sp == "" {
		return a.RenderTemplate("App/Index.html")
	}

	dbcfg := EnvConfig()
	h, e := dbcfg.Handle()
	if e != nil {
		log.Println(e)
		a.RenderArgs["action"] = "Error"
		a.RenderArgs["errorCode"] = ERROR_DB_CFG
		return a.RenderTemplate("App/Index.html")
	}

	// fetch retailer from cache if available, or use database and update cache
	var servicePoint *ServicePoint
	if e := cache.Get("sp_"+sp, &servicePoint); e != nil {
		servicePoint, e = h.ServicePoint(sp)
		if e != nil {
			log.Println(e)
			a.RenderArgs["action"] = "Error"
			a.RenderArgs["errorCode"] = ERROR_RETAILER_QUERY
			return a.RenderTemplate("App/Index.html")
		}
		go cache.Set("sp_"+sp, servicePoint, 30*time.Minute)
	}

	cocfg := &urbit.CheckoutConfig{
		Token:          servicePoint.Token,
		ConsumerSecret: servicePoint.ConsumerSecret,
	}

	// get checkout status
	cs, e := cocfg.CheckoutStatus(id)
	if e != nil {
		log.Println(e)
		a.RenderArgs["action"] = "Error"
		a.RenderArgs["errorCode"] = ERROR_CHECKOUT_STATUS
		return a.RenderTemplate("App/Index.html")
	}

	m := map[string]string{
		"environment": os.Getenv("ENVIRONMENT"),
		"action":      "Thankyou",
		"success": func(s string) string {
			if strings.ToLower(s) == "complete" {
				return "true"
			}
			return "false"
		}(cs.CheckoutStatus),
		"valid":        "true",
		"name":         *cs.Consumer.FirstName,
		"address":      cs.Delivery.Street,
		"deliveryType": "Specific", //fmt.Sprintf("%v", cs.DeliveryType),
		"deliveryTime": func(s string) string {
			l, e := time.LoadLocation("Europe/Stockholm")
			// parse delivery time
			t, e := time.ParseInLocation(time.RFC3339, s, l)
			if e != nil {
				return ""
			}
			return t.Format("2006-01-02 kl 15:04")
		}(cs.DeliveryExpectedAt),
	}

	for k, v := range m {
		a.RenderArgs[k] = v
	}

	return a.RenderTemplate("App/Index.html")
}
