package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
)

// AddressDto address dto
// swagger:model AddressDto
type AddressDto struct {

	// city
	City string `json:"city,omitempty"`

	// country
	Country string `json:"country,omitempty"`

	// country code
	CountryCode string `json:"countryCode,omitempty"`

	// post code
	PostCode string `json:"postCode,omitempty"`

	// street1
	Street1 string `json:"street1,omitempty"`

	// street2
	Street2 string `json:"street2,omitempty"`
}

// Validate validates this address dto
func (m *AddressDto) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
