package businesslocation_v1_servicepoint

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/urbitassociates/postnord-direct/app/clients/postnord/servicepoint/models"
)

// FindByPostalCodeReader is a Reader for the FindByPostalCode structure.
type FindByPostalCodeReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *FindByPostalCodeReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewFindByPostalCodeOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 400:
		result := NewFindByPostalCodeBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewFindByPostalCodeInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewFindByPostalCodeOK creates a FindByPostalCodeOK with default headers values
func NewFindByPostalCodeOK() *FindByPostalCodeOK {
	return &FindByPostalCodeOK{}
}

/*FindByPostalCodeOK handles this case with default header values.

No response was specified
*/
type FindByPostalCodeOK struct {
	Payload *models.ResponseDto
}

func (o *FindByPostalCodeOK) Error() string {
	return fmt.Sprintf("[GET /rest/businesslocation/v1/servicepoint/findByPostalCode.{returntype}][%d] findByPostalCodeOK  %+v", 200, o.Payload)
}

func (o *FindByPostalCodeOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ResponseDto)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewFindByPostalCodeBadRequest creates a FindByPostalCodeBadRequest with default headers values
func NewFindByPostalCodeBadRequest() *FindByPostalCodeBadRequest {
	return &FindByPostalCodeBadRequest{}
}

/*FindByPostalCodeBadRequest handles this case with default header values.

The server did not understand or could not validate the input parameters. More information about the cause of the error is available in the compositeFault object.
*/
type FindByPostalCodeBadRequest struct {
}

func (o *FindByPostalCodeBadRequest) Error() string {
	return fmt.Sprintf("[GET /rest/businesslocation/v1/servicepoint/findByPostalCode.{returntype}][%d] findByPostalCodeBadRequest ", 400)
}

func (o *FindByPostalCodeBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewFindByPostalCodeInternalServerError creates a FindByPostalCodeInternalServerError with default headers values
func NewFindByPostalCodeInternalServerError() *FindByPostalCodeInternalServerError {
	return &FindByPostalCodeInternalServerError{}
}

/*FindByPostalCodeInternalServerError handles this case with default header values.

The server experienced a runtime exception while processing the request. Try again later or contact customer support.
*/
type FindByPostalCodeInternalServerError struct {
}

func (o *FindByPostalCodeInternalServerError) Error() string {
	return fmt.Sprintf("[GET /rest/businesslocation/v1/servicepoint/findByPostalCode.{returntype}][%d] findByPostalCodeInternalServerError ", 500)
}

func (o *FindByPostalCodeInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
