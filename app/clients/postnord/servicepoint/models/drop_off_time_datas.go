package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"

	"github.com/go-openapi/errors"
)

// DropOffTimeDatas drop off time datas
// swagger:model DropOffTimeDatas
type DropOffTimeDatas struct {

	// drop off time data
	DropOffTimeData []*DropOffTimeDataDto `json:"dropOffTimeData,omitempty"`
}

// Validate validates this drop off time datas
func (m *DropOffTimeDatas) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateDropOffTimeData(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *DropOffTimeDatas) validateDropOffTimeData(formats strfmt.Registry) error {

	if swag.IsZero(m.DropOffTimeData) { // not required
		return nil
	}

	for i := 0; i < len(m.DropOffTimeData); i++ {

		if swag.IsZero(m.DropOffTimeData[i]) { // not required
			continue
		}

		if m.DropOffTimeData[i] != nil {

			if err := m.DropOffTimeData[i].Validate(formats); err != nil {
				return err
			}
		}

	}

	return nil
}
