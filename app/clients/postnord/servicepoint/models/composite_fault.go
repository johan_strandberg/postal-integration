package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"

	"github.com/go-openapi/errors"
)

// CompositeFault composite fault
// swagger:model CompositeFault
type CompositeFault struct {

	// faults
	Faults []*Fault `json:"faults,omitempty"`
}

// Validate validates this composite fault
func (m *CompositeFault) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateFaults(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *CompositeFault) validateFaults(formats strfmt.Registry) error {

	if swag.IsZero(m.Faults) { // not required
		return nil
	}

	for i := 0; i < len(m.Faults); i++ {

		if swag.IsZero(m.Faults[i]) { // not required
			continue
		}

		if m.Faults[i] != nil {

			if err := m.Faults[i].Validate(formats); err != nil {
				return err
			}
		}

	}

	return nil
}
