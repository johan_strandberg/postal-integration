package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
)

// CoordinateDto coordinate dto
// swagger:model CoordinateDto
type CoordinateDto struct {

	// country code
	CountryCode string `json:"countryCode,omitempty"`

	// easting
	Easting float64 `json:"easting,omitempty"`

	// northing
	Northing float64 `json:"northing,omitempty"`

	// sr Id
	SrID string `json:"srId,omitempty"`
}

// Validate validates this coordinate dto
func (m *CoordinateDto) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
