package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetGenerateItemIDParams creates a new GetGenerateItemIDParams object
// with the default values initialized.
func NewGetGenerateItemIDParams() *GetGenerateItemIDParams {
	var (
		contentTypeDefault = string("application/json")
	)
	return &GetGenerateItemIDParams{
		ContentType: contentTypeDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetGenerateItemIDParamsWithTimeout creates a new GetGenerateItemIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetGenerateItemIDParamsWithTimeout(timeout time.Duration) *GetGenerateItemIDParams {
	var (
		contentTypeDefault = string("application/json")
	)
	return &GetGenerateItemIDParams{
		ContentType: contentTypeDefault,

		timeout: timeout,
	}
}

// NewGetGenerateItemIDParamsWithContext creates a new GetGenerateItemIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetGenerateItemIDParamsWithContext(ctx context.Context) *GetGenerateItemIDParams {
	var (
		contentTypeDefault = string("application/json")
	)
	return &GetGenerateItemIDParams{
		ContentType: contentTypeDefault,

		Context: ctx,
	}
}

/*GetGenerateItemIDParams contains all the parameters to send to the API endpoint
for the get generate item ID operation typically these are written to a http.Request
*/
type GetGenerateItemIDParams struct {

	/*ContentType
	  Content-Type&#58; 'json' required for this GET request...

	*/
	ContentType string
	/*Apikey
	  The unique consumer (client) identifier 32 characters

	*/
	Apikey string
	/*ItemID
	  ItemId for the batch

	*/
	ItemID string

	timeout time.Duration
	Context context.Context
}

// WithTimeout adds the timeout to the get generate item ID params
func (o *GetGenerateItemIDParams) WithTimeout(timeout time.Duration) *GetGenerateItemIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get generate item ID params
func (o *GetGenerateItemIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get generate item ID params
func (o *GetGenerateItemIDParams) WithContext(ctx context.Context) *GetGenerateItemIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get generate item ID params
func (o *GetGenerateItemIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithContentType adds the contentType to the get generate item ID params
func (o *GetGenerateItemIDParams) WithContentType(contentType string) *GetGenerateItemIDParams {
	o.SetContentType(contentType)
	return o
}

// SetContentType adds the contentType to the get generate item ID params
func (o *GetGenerateItemIDParams) SetContentType(contentType string) {
	o.ContentType = contentType
}

// WithApikey adds the apikey to the get generate item ID params
func (o *GetGenerateItemIDParams) WithApikey(apikey string) *GetGenerateItemIDParams {
	o.SetApikey(apikey)
	return o
}

// SetApikey adds the apikey to the get generate item ID params
func (o *GetGenerateItemIDParams) SetApikey(apikey string) {
	o.Apikey = apikey
}

// WithItemID adds the itemID to the get generate item ID params
func (o *GetGenerateItemIDParams) WithItemID(itemID string) *GetGenerateItemIDParams {
	o.SetItemID(itemID)
	return o
}

// SetItemID adds the itemId to the get generate item ID params
func (o *GetGenerateItemIDParams) SetItemID(itemID string) {
	o.ItemID = itemID
}

// WriteToRequest writes these params to a swagger request
func (o *GetGenerateItemIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	r.SetTimeout(o.timeout)
	var res []error

	// header param Content-Type
	if err := r.SetHeaderParam("Content-Type", o.ContentType); err != nil {
		return err
	}

	// query param apikey
	qrApikey := o.Apikey
	qApikey := qrApikey
	if qApikey != "" {
		if err := r.SetQueryParam("apikey", qApikey); err != nil {
			return err
		}
	}

	// path param itemId
	if err := r.SetPathParam("itemId", o.ItemID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
