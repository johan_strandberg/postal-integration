package postnord

import (
	"fmt"
	"github.com/go-openapi/runtime"
	"golang.org/x/oauth2/clientcredentials"
	"log"
	"os"
	collectcodeClient "github.com/urbitassociates/postnord-direct/app/clients/postnord/collectcode/client"
	collectcodeOperations "github.com/urbitassociates/postnord-direct/app/clients/postnord/collectcode/client/operations"
	httptransport "github.com/go-openapi/runtime/client"
)

type AuthConfig struct {
	// ApiKey is the token from the Postnord developer portal
	ApiKey string

	// ClientID is the application's ID.
	ClientID string

	// ClientSecret is the application's secret.
	ClientSecret string

	// TokenURL is the resource server's token endpoint
	// URL. This is a constant specific to each server.
	TokenURL string

	// Scope specifies optional requested permissions.
	Scopes []string
}

func NewAuthConfig() *AuthConfig {
	return &AuthConfig{
		ApiKey:       os.Getenv("POSTNORD_API_KEY"),
		ClientID:     os.Getenv("POSTNORD_OAUTH2_CLIENT_ID"),
		ClientSecret: os.Getenv("POSTNORD_OAUTH2_CLIENT_SECRET"),
		Scopes:       []string{"all"},
		TokenURL:     os.Getenv("POSTNORD_OAUTH2_TOKEN_URL"),
	}
}

func (ac *AuthConfig) AccessToken() (string, error) {
	cfg := &clientcredentials.Config{
		ClientID:     ac.ClientID,
		ClientSecret: ac.ClientSecret,
		Scopes:       ac.Scopes,
		TokenURL:     ac.TokenURL,
	}
	t, e := cfg.Token(nil)

	if e != nil {
		log.Panicln(e)
	}
	if !t.Valid() {
		log.Fatalf("token invalid. got: %#v\n", t)
	}
	if t.TokenType != "bearer" {
		log.Fatalf("token type = %q; want %q\n", t.TokenType, "bearer")
	}
	return t.AccessToken, e
}

func (ac *AuthConfig) BearerToken() (runtime.ClientAuthInfoWriter, error) {
	t, e := ac.AccessToken()
	if e != nil {
		return nil, e
	}
	return httptransport.BearerToken(t), nil
}

func (ac *AuthConfig) CollectCode(id string) (string, error) {
	// create the API client
	//c := client.Default
	t := httptransport.New(
		os.Getenv("POSTNORD_CC_API_HOST"),
		os.Getenv("POSTNORD_CC_API_BASEPATH"),
		[]string{os.Getenv("POSTNORD_CC_API_SCHEME")})
	t.Debug = true
	c := collectcodeClient.New(t, nil)

	// auth
	bt, e := ac.BearerToken()
	if e != nil {
		return "", nil
	}

	// set req params
	p := collectcodeOperations.NewGetGenerateItemIDParams()
	p.SetContentType("application/json")
	p.SetApikey(ac.ApiKey)
	p.SetItemID(id)

	// get collect code
	r, e := c.Operations.GetGenerateItemID(p, bt)

	if e != nil {
		log.Fatalln(e)
		return "", e
	}

	return r.Payload.CollectCode, nil
}

func RefID(cc string, id string) string {
	return fmt.Sprintf("POSTNORD_%v_%v", cc, id)
}
