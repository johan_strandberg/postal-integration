package stores

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetStoresParams creates a new GetStoresParams object
// with the default values initialized.
func NewGetStoresParams() *GetStoresParams {

	return &GetStoresParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetStoresParamsWithTimeout creates a new GetStoresParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetStoresParamsWithTimeout(timeout time.Duration) *GetStoresParams {

	return &GetStoresParams{

		timeout: timeout,
	}
}

// NewGetStoresParamsWithContext creates a new GetStoresParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetStoresParamsWithContext(ctx context.Context) *GetStoresParams {

	return &GetStoresParams{

		Context: ctx,
	}
}

/*GetStoresParams contains all the parameters to send to the API endpoint
for the get stores operation typically these are written to a http.Request
*/
type GetStoresParams struct {
	timeout time.Duration
	Context context.Context
}

// WithTimeout adds the timeout to the get stores params
func (o *GetStoresParams) WithTimeout(timeout time.Duration) *GetStoresParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get stores params
func (o *GetStoresParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get stores params
func (o *GetStoresParams) WithContext(ctx context.Context) *GetStoresParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get stores params
func (o *GetStoresParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WriteToRequest writes these params to a swagger request
func (o *GetStoresParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	r.SetTimeout(o.timeout)
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
