package orders

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/urbitassociates/postnord-direct/app/clients/urbit/retailer/models"
)

// NewPostOrdersParams creates a new PostOrdersParams object
// with the default values initialized.
func NewPostOrdersParams() *PostOrdersParams {
	var ()
	return &PostOrdersParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostOrdersParamsWithTimeout creates a new PostOrdersParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostOrdersParamsWithTimeout(timeout time.Duration) *PostOrdersParams {
	var ()
	return &PostOrdersParams{

		timeout: timeout,
	}
}

// NewPostOrdersParamsWithContext creates a new PostOrdersParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostOrdersParamsWithContext(ctx context.Context) *PostOrdersParams {
	var ()
	return &PostOrdersParams{

		Context: ctx,
	}
}

/*PostOrdersParams contains all the parameters to send to the API endpoint
for the post orders operation typically these are written to a http.Request
*/
type PostOrdersParams struct {

	/*Body*/
	Body *models.Order

	timeout time.Duration
	Context context.Context
}

// WithTimeout adds the timeout to the post orders params
func (o *PostOrdersParams) WithTimeout(timeout time.Duration) *PostOrdersParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post orders params
func (o *PostOrdersParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post orders params
func (o *PostOrdersParams) WithContext(ctx context.Context) *PostOrdersParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post orders params
func (o *PostOrdersParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithBody adds the body to the post orders params
func (o *PostOrdersParams) WithBody(body *models.Order) *PostOrdersParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the post orders params
func (o *PostOrdersParams) SetBody(body *models.Order) {
	o.Body = body
}

// WriteToRequest writes these params to a swagger request
func (o *PostOrdersParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	r.SetTimeout(o.timeout)
	var res []error

	if o.Body == nil {
		o.Body = new(models.Order)
	}

	if err := r.SetBodyParam(o.Body); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
