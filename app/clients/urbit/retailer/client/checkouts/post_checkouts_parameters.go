package checkouts

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/urbitassociates/postnord-direct/app/clients/urbit/retailer/models"
)

// NewPostCheckoutsParams creates a new PostCheckoutsParams object
// with the default values initialized.
func NewPostCheckoutsParams() *PostCheckoutsParams {
	var ()
	return &PostCheckoutsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostCheckoutsParamsWithTimeout creates a new PostCheckoutsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostCheckoutsParamsWithTimeout(timeout time.Duration) *PostCheckoutsParams {
	var ()
	return &PostCheckoutsParams{

		timeout: timeout,
	}
}

// NewPostCheckoutsParamsWithContext creates a new PostCheckoutsParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostCheckoutsParamsWithContext(ctx context.Context) *PostCheckoutsParams {
	var ()
	return &PostCheckoutsParams{

		Context: ctx,
	}
}

/*PostCheckoutsParams contains all the parameters to send to the API endpoint
for the post checkouts operation typically these are written to a http.Request
*/
type PostCheckoutsParams struct {

	/*Body*/
	Body *models.Checkout

	timeout time.Duration
	Context context.Context
}

// WithTimeout adds the timeout to the post checkouts params
func (o *PostCheckoutsParams) WithTimeout(timeout time.Duration) *PostCheckoutsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post checkouts params
func (o *PostCheckoutsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post checkouts params
func (o *PostCheckoutsParams) WithContext(ctx context.Context) *PostCheckoutsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post checkouts params
func (o *PostCheckoutsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithBody adds the body to the post checkouts params
func (o *PostCheckoutsParams) WithBody(body *models.Checkout) *PostCheckoutsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the post checkouts params
func (o *PostCheckoutsParams) SetBody(body *models.Checkout) {
	o.Body = body
}

// WriteToRequest writes these params to a swagger request
func (o *PostCheckoutsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	r.SetTimeout(o.timeout)
	var res []error

	if o.Body == nil {
		o.Body = new(models.Checkout)
	}

	if err := r.SetBodyParam(o.Body); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
