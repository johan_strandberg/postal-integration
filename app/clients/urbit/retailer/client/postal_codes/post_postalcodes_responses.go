package postal_codes

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/urbitassociates/postnord-direct/app/clients/urbit/retailer/models"
)

// PostPostalcodesReader is a Reader for the PostPostalcodes structure.
type PostPostalcodesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostPostalcodesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPostPostalcodesOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewPostPostalcodesUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewPostPostalcodesNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostPostalcodesOK creates a PostPostalcodesOK with default headers values
func NewPostPostalcodesOK() *PostPostalcodesOK {
	return &PostPostalcodesOK{}
}

/*PostPostalcodesOK handles this case with default header values.

Status 200
*/
type PostPostalcodesOK struct {
	Payload *models.PostalCodeValidated
}

func (o *PostPostalcodesOK) Error() string {
	return fmt.Sprintf("[POST /postalcodes][%d] postPostalcodesOK  %+v", 200, o.Payload)
}

func (o *PostPostalcodesOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PostalCodeValidated)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostPostalcodesUnauthorized creates a PostPostalcodesUnauthorized with default headers values
func NewPostPostalcodesUnauthorized() *PostPostalcodesUnauthorized {
	return &PostPostalcodesUnauthorized{}
}

/*PostPostalcodesUnauthorized handles this case with default header values.

Unauthorized
*/
type PostPostalcodesUnauthorized struct {
	Payload *models.Unauthorized
}

func (o *PostPostalcodesUnauthorized) Error() string {
	return fmt.Sprintf("[POST /postalcodes][%d] postPostalcodesUnauthorized  %+v", 401, o.Payload)
}

func (o *PostPostalcodesUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Unauthorized)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostPostalcodesNotFound creates a PostPostalcodesNotFound with default headers values
func NewPostPostalcodesNotFound() *PostPostalcodesNotFound {
	return &PostPostalcodesNotFound{}
}

/*PostPostalcodesNotFound handles this case with default header values.

Status 404
*/
type PostPostalcodesNotFound struct {
	Payload *models.LocationOutsideDeliveryZone
}

func (o *PostPostalcodesNotFound) Error() string {
	return fmt.Sprintf("[POST /postalcodes][%d] postPostalcodesNotFound  %+v", 404, o.Payload)
}

func (o *PostPostalcodesNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.LocationOutsideDeliveryZone)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
