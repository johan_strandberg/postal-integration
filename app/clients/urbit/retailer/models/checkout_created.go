package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
)

// CheckoutCreated checkout created
// swagger:model CheckoutCreated
type CheckoutCreated struct {

	// Location to the created checkout
	Location string `json:"location,omitempty"`
}

// Validate validates this checkout created
func (m *CheckoutCreated) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
