package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// Address address
// swagger:model Address
type Address struct {

	// care of
	CareOf string `json:"care_of,omitempty"`

	// city
	// Required: true
	City *string `json:"city"`

	// company name
	CompanyName string `json:"company_name,omitempty"`

	// country
	// Required: true
	Country *string `json:"country"`

	// postal code
	// Required: true
	PostalCode *string `json:"postal_code"`

	// street
	// Required: true
	Street *string `json:"street"`

	// street2
	Street2 string `json:"street2,omitempty"`
}

// Validate validates this address
func (m *Address) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCity(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateCountry(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validatePostalCode(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateStreet(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Address) validateCity(formats strfmt.Registry) error {

	if err := validate.Required("city", "body", m.City); err != nil {
		return err
	}

	return nil
}

func (m *Address) validateCountry(formats strfmt.Registry) error {

	if err := validate.Required("country", "body", m.Country); err != nil {
		return err
	}

	return nil
}

func (m *Address) validatePostalCode(formats strfmt.Registry) error {

	if err := validate.Required("postal_code", "body", m.PostalCode); err != nil {
		return err
	}

	return nil
}

func (m *Address) validateStreet(formats strfmt.Registry) error {

	if err := validate.Required("street", "body", m.Street); err != nil {
		return err
	}

	return nil
}
