package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// Consumer consumer
// swagger:model Consumer
type Consumer struct {

	// address
	// Required: true
	Address *Address `json:"address"`

	// Cellphone number of consumer. Must include international prefix without spaces. e.g. +46731234567
	// Required: true
	CellPhone *string `json:"cell_phone"`

	// Optional comment
	ConsumerComment string `json:"consumer_comment,omitempty"`

	// E-mail address of consumer
	// Required: true
	Email *string `json:"email"`

	// irst name of consumer
	// Required: true
	FirstName *string `json:"first_name"`

	// Last name of consumer
	// Required: true
	LastName *string `json:"last_name"`
}

// Validate validates this consumer
func (m *Consumer) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAddress(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateCellPhone(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateEmail(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateFirstName(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateLastName(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Consumer) validateAddress(formats strfmt.Registry) error {

	if err := validate.Required("address", "body", m.Address); err != nil {
		return err
	}

	if m.Address != nil {

		if err := m.Address.Validate(formats); err != nil {
			return err
		}
	}

	return nil
}

func (m *Consumer) validateCellPhone(formats strfmt.Registry) error {

	if err := validate.Required("cell_phone", "body", m.CellPhone); err != nil {
		return err
	}

	return nil
}

func (m *Consumer) validateEmail(formats strfmt.Registry) error {

	if err := validate.Required("email", "body", m.Email); err != nil {
		return err
	}

	return nil
}

func (m *Consumer) validateFirstName(formats strfmt.Registry) error {

	if err := validate.Required("first_name", "body", m.FirstName); err != nil {
		return err
	}

	return nil
}

func (m *Consumer) validateLastName(formats strfmt.Registry) error {

	if err := validate.Required("last_name", "body", m.LastName); err != nil {
		return err
	}

	return nil
}
