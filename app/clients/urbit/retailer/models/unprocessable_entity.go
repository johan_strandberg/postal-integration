package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"

	"github.com/go-openapi/errors"
)

// UnprocessableEntity unprocessable entity
// swagger:model UnprocessableEntity
type UnprocessableEntity struct {

	// "422"
	Code string `json:"code,omitempty"`

	// Your request was understood but contained invalid parameters.
	Description string `json:"description,omitempty"`

	// All properties are not valid. Check the invalid_properties for more information.
	DeveloperMessage string `json:"developer_message,omitempty"`

	// UnprocessableEntity
	Exception string `json:"exception,omitempty"`

	// invalid properties
	InvalidProperties []*InvalidProperty `json:"invalid_properties,omitempty"`

	// Unprocessable entity
	Message string `json:"message,omitempty"`

	// null
	MoreInfo string `json:"more_info,omitempty"`

	// 422
	Status string `json:"status,omitempty"`
}

// Validate validates this unprocessable entity
func (m *UnprocessableEntity) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateInvalidProperties(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *UnprocessableEntity) validateInvalidProperties(formats strfmt.Registry) error {

	if swag.IsZero(m.InvalidProperties) { // not required
		return nil
	}

	for i := 0; i < len(m.InvalidProperties); i++ {

		if swag.IsZero(m.InvalidProperties[i]) { // not required
			continue
		}

		if m.InvalidProperties[i] != nil {

			if err := m.InvalidProperties[i].Validate(formats); err != nil {
				return err
			}
		}

	}

	return nil
}
