package urbit

import (
	"os"
	"fmt"
	"strconv"
	"encoding/json"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/urbitassociates/urbit-go/uwa"
	"github.com/urbitassociates/postnord-direct/app/clients/urbit/retailer/client"
	"github.com/urbitassociates/postnord-direct/app/clients/urbit/retailer/models"
	"github.com/urbitassociates/postnord-direct/app/clients/urbit/retailer/client/checkouts"
)

type CheckoutConfig struct {
	OrderID            string
	CancelURL          string
	ReturnURL          string
	ArticleDescription string
	ArticleRefID       string
	Token              string
	ConsumerSecret     string
}

func NewCheckoutConfig() *CheckoutConfig {
	return &CheckoutConfig{}
}

func (cc *CheckoutConfig) Checkout() (*models.CheckoutCreated, error) {
	// create the Urbit Retailer API client
	//c := client.Default
	t := httptransport.New(
		os.Getenv("RETAILER_API_HOST"),
		os.Getenv("RETAILER_API_BASEPATH"),
		[]string{os.Getenv("RETAILER_API_SCHEME")})
	t.Debug, _ = strconv.ParseBool(os.Getenv("DEBUG"))
	c := client.New(t, nil)

	// set req params
	p := checkouts.NewPostCheckoutsParams()
	var (
		language = "sv"
		currency = "SEK"
		article = &models.Article{
			Description: cc.ArticleDescription,
			Price: 0,
			Quantity: 1,
			RetailerReferenceID: cc.ArticleRefID,
			VatPercentage: 25,
		}
	)
	co := &models.Checkout{
		RetailerReferenceID: cc.OrderID,
		Language: language,
		Currency: &currency,
		CancelURL: &cc.CancelURL,
		ReturnURL: &cc.ReturnURL,
	}
	co.Articles = append(co.Articles, article)
	p.SetBody(co)

	// marshal payload to json byte slice
	b, e := json.Marshal(p.Body)
	if e != nil {
		return nil, e
	}

	// append newline char so that the hash of this will match the request body
	// because https://git.io/vXr1P and http://stackoverflow.com/a/36320146
	b = append(b, byte('\n'))

	// request url
	u := fmt.Sprintf(
		"%v://%v%v%v",
		os.Getenv("RETAILER_API_SCHEME"),
		os.Getenv("RETAILER_API_HOST"),
		os.Getenv("RETAILER_API_BASEPATH"),
		"/checkouts",
	)
	// create UWA header
	h := uwa.AuthorizationHeader(
		cc.Token,
		cc.ConsumerSecret,
		u,
		"POST",
		b,
	)
	ah := httptransport.APIKeyAuth("Authorization", "header", h)

	// create checkout
	r, e := c.Checkouts.PostCheckouts(p, ah)

	if e != nil {
		return nil, e
	}

	return r.Payload, nil
}

func (cc *CheckoutConfig) CheckoutStatus(id string) (*models.CheckoutStatus, error) {
	// create the Urbit Retailer API client
	//c := client.Default
	t := httptransport.New(
		os.Getenv("RETAILER_API_HOST"),
		os.Getenv("RETAILER_API_BASEPATH"),
		[]string{os.Getenv("RETAILER_API_SCHEME")})
	t.Debug, _ = strconv.ParseBool(os.Getenv("DEBUG"))
	c := client.New(t, nil)

	// set req params
	p := checkouts.NewGetCheckoutsReferenceIDReferenceIDParams()
	p.ReferenceID = id

	// request url
	u := fmt.Sprintf(
		"%v://%v%v%v",
		os.Getenv("RETAILER_API_SCHEME"),
		os.Getenv("RETAILER_API_HOST"),
		os.Getenv("RETAILER_API_BASEPATH"),
		"/checkouts/reference-id/"+id,
	)
	// create UWA header
	var b []byte
	h := uwa.AuthorizationHeader(
		cc.Token,
		cc.ConsumerSecret,
		u,
		"GET",
		b,
	)
	ah := httptransport.APIKeyAuth("Authorization", "header", h)

	// get checkout status
	r, e := c.Checkouts.GetCheckoutsReferenceIDReferenceID(p, ah)

	if e != nil {
		return nil, e
	}

	return r.Payload, nil
}
