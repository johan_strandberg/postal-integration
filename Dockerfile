FROM golang:1.7-alpine

MAINTAINER Johan Strandberg <johan@urbit.com>

ENV PATH="${PATH}:${GOROOT}/bin:${GOPATH}/bin"

ENV GOPATH="/go"
ENV GOROOT="/usr/local/go"
ENV PACKAGE_PATH="github.com/urbitassociates/postnord-direct"

# Install dependencies & remove unneeded ones
RUN apk --no-cache --update --repository http://dl-3.alpinelinux.org/alpine/edge/community/ add \
    curl \
    git \
    glide \
&& apk --no-cache del \
    wget

# Install revel
RUN go get \
    github.com/revel/revel \
    github.com/revel/cmd/revel

WORKDIR ${GOPATH}/src/${PACKAGE_PATH}

# Build & Run
COPY ./ ./
RUN revel build $PACKAGE_PATH /revel
CMD ["/bin/sh", "/revel/run.sh"]

EXPOSE 9000
