CREATE TABLE `service_points` (
  `id` int(11) unsigned NOT NULL,
  `name` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `consumer_secret` char(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `token` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `pickup_location_guid` char(64) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
