Urbit Postnord Direct
==============

Postnord Direct is written in [Golang] using the [Revel] web framework.

API clients for Postnord and Urbit are auto-generated from [Swagger] files using [go-swagger].

The app runs inside an [Alpine]-based [Docker] container.

## Get started
First, clone this repo to your $GOPATH:
```
git clone https://github.com/urbitassociates/postnord-direct $GOPATH/src/github.com/urbitassociates/postnord-direct
```
Then run the app.
### Using Docker
#### Build
```
docker build -t quay.io/urbit/postnord-direct .
```
#### Run
```
$ docker run \
    -p 9000:9000 \
    --env-file .env \
    quay.io/urbit/postnord-direct
```
### Using Golang
First, install [Glide]:
```
curl https://glide.sh/get | sh

```
Or (macOS):
```
brew install glide
```
Then install dependencies:
```
glide install
```
And:
```
go get github.com/revel/revel
go get github.com/revel/cmd/revel

```
Start the app
```
revel run github.com/urbitassociates/postnord-direct
```
Then open http://localhost:9000 in a web browser.

## Contributing
Write **imperative, present tense** commit messages when contributing.

_"Change"_, not _"changed"_ or _"changes"_; _"add"_, not _"added"_ etc.


[Golang]: https://golang.org/
[Glide]: https://glide.sh/
[Revel]: https://revel.github.io/
[Docker]: https://www.docker.com/
[Alpine]: https://hub.docker.com/_/alpine/
[Swagger]: http://swagger.io/
[go-swagger]: https://github.com/go-swagger/go-swagger
